pragma solidity ^0.4.24;

import "./ERC721Enumerable.sol";
import "./Ownable.sol";

contract PIBComment is ERC721Enumerable, Ownable {

    struct Comment {
        // string  hash; use tokenID as hash
        uint id;
        uint postId;  // Title of the Image
        uint ownerId; 
        string entityType;
    }

    Comment[] public comments; // First Item has Index 0
    address public owner;    
    

    // /**
    // * @dev Mints a token to an address with a tokenURI.
    // * @param _to address of the future owner of the token
    // * @param id comment Id of the token to query the owner of
    // */
    // function mint(address _to, uint id) public onlyOwner {
    //     _mint(_to, id);
    // }

    /**
    * @dev Mints a token to an address with a tokenURI.
    * @param _to address of the future owner of the token
    * @param id comment ID of the token to query the owner of
    */
    function mintPibComment(address _to, uint id, uint postId, uint ownerId, string entityType) public onlyOwner {
        // require(owner == msg.sender); // Only the Owner can create Items
        
        comments.push(Comment(id,postId,ownerId,entityType));
        _mint(_to, id);
    }
    
    function renounceOwnership() public onlyOwner {
        
    }
    /**
    * @dev Gets the owner of the specified token ID
    * @param postId post ID of the token to query the owner of
    * @return owner address currently marked as the owner of the given token ID
    */
    function ownerOfHash(uint postId) public view returns (address) {
        return super.ownerOf(postId);
    }
}