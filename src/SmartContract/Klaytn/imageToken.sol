pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC721/ERC721Enumerable.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

contract ImageToken is ERC721Enumerable, Ownable {

    uint256 private price;
    uint256 private discount = 0;
    uint256 private limit;
    uint8 public constant decimals = 18;

    ERC20 public pibbleToken;

    constructor(address _pibbleToken, uint _price, uint _discount, uint _limit) ERC721Enumerable() public {
        pibbleToken = ERC20(_pibbleToken);
        price = _price * (10 ** uint256(decimals));
        discount = _discount;
        limit = _limit;
    }

    /**
    * @dev Mints a token to an address with a tokenURI.
    * @param _to address of the future owner of the token
    */
    function mintTo(address _to) public onlyOwner {
        require(balanceOf(_to) == 0, "Already has token");
        require(totalSupply() < limit, "All tokens were sold");

        uint _price = getPrice();

        pibbleToken.transferFrom(_to, msg.sender, _price);
        
        uint256 newTokenId = _getNextTokenId();
        _mint(_to, newTokenId);
    }

    /**
    * @dev calculates the next token ID based on totalSupply
    * @return uint256 for the next token ID
    */
    function _getNextTokenId() private view returns (uint256) {
        return totalSupply().add(1); 
    }

    /**
    * @dev calculates the price based on discount
    * @return uint price with discount 
    */
    function getPrice() public view returns (uint) {
        return (price - price * discount / 100) ; 
    }

    /**
    * @dev Set new discount
    * @param _discount uint discount value
    * @return uint price with discount 
    */
    function setDiscount(uint _discount) public onlyOwner {
        discount = _discount; 
    }

    /**
    * @dev Set new limit. Can`t be less or equal to previous. Can`t be less than already sold 
    * @param _limit uint limit value
    * @return uint limit 
    */
    function setLimit(uint _limit) public onlyOwner {
        require(_limit < limit && _limit >= totalSupply(), "Can`t be less or equal to previous. Can`t be less then already sold.");
        limit = _limit; 
    }

    /**
    * @dev Get limit
    * @return uint price with discount 
    */
    function getLimit() public view returns (uint) {
        return limit;
    }
}