pragma solidity ^0.4.24;

import "./flatten_721.sol"; // ERC721Enumerable
import "./Ownable.sol";

contract PIBGameScoreTx is ERC721Enumerable, Ownable {

    struct ScoreTransaction {
        uint id;
        uint userId; 
        string gameId;
        uint playType; 
        uint score;
        string playedAt;
        string description;  
    }

    ScoreTransaction[] public scoreTransactions; // First Item has Index 0
    address public owner;    
    

    /**
    * @dev Mints a token to an address with a tokenURI.
    * @param id score ID of the token to query the owner of
    */
    function mintScoreTx(
        uint id,
        uint userId,
        string gameId,
        uint playType, 
        uint score,
        string playedAt,
        string description
        )    
        public 
        {
            scoreTransactions.push(ScoreTransaction( id, userId, gameId, playType, score, playedAt, description));
            _mint(this, id);
        }
    
    function renounceOwnership() public onlyOwner {
        
    }
    
}