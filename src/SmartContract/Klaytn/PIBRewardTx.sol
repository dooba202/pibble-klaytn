pragma solidity ^0.4.24;

import "./flatten_721.sol"; // ERC721Enumerable
import "./Ownable.sol";

contract PIBRewardTx is ERC721Enumerable, Ownable {

    struct RewardTransaction {
        uint id;
        uint fromValue;  
        uint toValue; 
        uint fromUser;  
        uint toUser; 
        address fromUserAddr;
        address toUserAddr;
        uint fromCoin;  
        uint toCoin; 
        uint rewardType;
    }

    RewardTransaction[] public rewardTransactions; // First Item has Index 0
    address public owner;    
    

    /**
    * @dev Mints a token to an address with a tokenURI.
    * @param toUserAddr address of the future owner of the token
    * @param id comment ID of the token to query the owner of
    */
    function mintPibRewardTx(
          uint id
        , uint fromValue, uint toValue
        , uint fromUser, uint toUser
        , address fromUserAddr, address toUserAddr
        , uint fromCoin ,uint toCoin
        , uint rewardType ) public onlyOwner {
        // require(owner == msg.sender); // Only the Owner can create Items
        
        rewardTransactions.push(RewardTransaction( id, fromValue, toValue, fromUser, toUser, fromUserAddr, toUserAddr, fromCoin, toCoin, rewardType ));
        _mint(toUserAddr, id);
    }
    
    function renounceOwnership() public onlyOwner {
        
    }
    
    // /**
    // * @dev Gets the owner of the specified token ID
    // * @param postId post ID of the token to query the owner of
    // * @return owner address currently marked as the owner of the given token ID
    // */
    // function ownerOfHash(uint postId) public view returns (address) {
    //     return super.ownerOf(postId);
    // }
}