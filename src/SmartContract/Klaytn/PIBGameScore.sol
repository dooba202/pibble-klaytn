pragma solidity ^0.4.24;

contract PIBGameScoreTx {

    struct ScoreTransaction {
        uint id;
        uint userId; 
        string gameId;
        uint playType; 
        uint score;
        string playedAt;
        string description;  
    }

    struct UserScore{
        uint256 score;
        address userAddr;
    }

    mapping(uint256 => UserScore[]) public UserScores;
    
    ScoreTransaction[] public scoreTransactions; 
    
    /**
    * @dev Mints a token to an address with a tokenURI.
    * @param id score ID of the token to query the owner of
    */
    function mintScoreTx(
        uint id,
        uint userId,
        string gameId,
        uint playType, 
        uint score,
        string playedAt,
        string description
        )    
        public 
        {
            scoreTransactions.push(ScoreTransaction( id, userId, gameId, playType, score, playedAt, description));
            UserScores[userId].push(UserScore(score,msg.sender));
        }
    function lastIndexOfUser(uint userId) public view returns(uint256){
        
        return UserScores[userId].length;
        
    }
        
        

    // function userLastScore(uint _userId, string _gameId, uint _playType) public view returns (uint, address ) {

    //     uint256 lastIndex = userScore[_userId][_gameId][_playType].sub(1);
    //     userScore lastScore = userScore[_userId][_gameId][_playType][lastIndex];
    //     return 1,msg.sender;
        
    // }

    // function userMaxScores(uint _userId, address _address, uint _id) public view returns (uint256) {
    //     return userScore[_userId][_address][_id];
        
    // }
    
    
}