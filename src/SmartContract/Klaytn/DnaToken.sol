pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC721/ERC721Enumerable.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract DnaToken is ERC721Enumerable, Ownable {

    /**
    * @dev Mints a token to an address with a tokenURI.
    * @param _to address of the future owner of the token
    * @param _newToken string ID of the token to query the owner of
    */
    function mint(address _to, string _newToken) public onlyOwner {
        uint tokenId = uint(keccak256(bytes(_newToken)));
        _mint(_to, tokenId);
    }

    /**
    * @dev Gets the owner of the specified token ID
    * @param _newToken string ID of the token to query the owner of
    * @return owner address currently marked as the owner of the given token ID
    */
    function ownerOfHash(string _newToken) public view returns (address) {
        uint tokenId = uint(keccak256(bytes(_newToken)));
        return super.ownerOf(tokenId);
    }
}